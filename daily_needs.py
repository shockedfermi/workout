#!/usr/bin/python3

"""
daily_needs.

Usage:
    daily_needs.py [ --mass=<mass>]

Options:

-m, --mass=<mass>  Amount of lean muscle mass to be put on per month [default: 0.6].

"""

import numpy as np
import docopt

if __name__ == '__main__':
    args = docopt.docopt(__doc__)
    print(args)
    args['--mass'] = float( args['--mass'] )





HEIGHT = 190.5 #cm
WEIGHT = 80.0  #kg
AGE    = 26 #years
ACTIVITY = {"Sedentary": 1.2,
            "Light":     1.375,
            "Moderate":  1.55,
            "Intense":   1.725,
            "Extreme":   1.9 }

#muscle gain
def kg_to_kcal( weight ):
    return np.array([5 * weight * 1000, 8 * weight * 1000 ])

def kcal_to_protein( kcal ):
    return kcal / 4.0

def kcal_to_lipid( kcal ):
    return kcal / 9.0

def kcal_to_carb( kcal ):
    return kcal / 4.0

#Revised Harris-Benedict Equation for men:
def BMR( W,
         H,
         A):
    
    return 13.397*W + 4.799*H - 5.677*A + 88.362

def TDEE( base,
          activity ):
    return base*ACTIVITY[activity]

def daily_energy( sustain_E,
                  weight_month ):
    return sustain_E + kg_to_kcal( weight_month ) / 30

#def daily_protein( W ):
#    return np.array([1.5 * W, 2.0 * W ])

def daily_protein( daily_E ):
    return np.array([kcal_to_protein( 0.12*np.mean( daily_E ) ), kcal_to_protein( 0.15*np.mean( daily_E ) )])

def daily_carb( daily_E ):
    return np.array([kcal_to_carb( 0.55*np.mean( daily_E ) ), kcal_to_carb( 0.65*np.mean( daily_E ) )])

def daily_lipid( daily_E ):
    return np.array([kcal_to_lipid( 0.30*np.mean( daily_E ) ), kcal_to_lipid( 0.35*np.mean( daily_E ) )])
    








#source: https://exceednutrition.com/nutrition-for-strength-training/
myBMR = BMR( WEIGHT, HEIGHT, AGE )
print( "Basal metabolic rate: ", myBMR )
print( "Total daily nutritional needs for gaining {0} kg lean mass per month: ".format( args['--mass']) )

for key, value in ACTIVITY.items():
    print( "\n{0} exercise".format( key ) )

    myTDEE = TDEE( myBMR, key )
    print( "\tTotal Daily Energy Expenditure: {0:.1f}".format( myTDEE ) )
    myDE = daily_energy( myTDEE, args['--mass'] )
    print( "\tRecommended daily energy intake: {0:.1f} - {1:.1f}".format( myDE[0], myDE[1] ) )
    myProteins = daily_protein( myDE )
    print( "\tRecommended daily protein intake:  {0:.1f} - {1:.1f}".format( myProteins[0], myProteins[1] ) )
    myLipids = daily_lipid( myDE )
    print( "\tRecommended daily lipid intake:  {0:.1f} - {1:.1f}".format( myLipids[0], myLipids[1] ) )
    myCarbs = daily_carb( myDE )
    print( "\tRecommended daily carbohydrate intake:  {0:.1f} - {1:.1f}".format( myCarbs[0], myCarbs[1] ) )

