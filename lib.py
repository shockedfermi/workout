import string, pickle


WEIGHTS      = pickle.load( open( "WEIGHTS.pickle",      "rb" ) )

def trim_ws( string ):
    return string.replace( ' ', '' )


class ExerciseSet:

    def __init__( self,
                  set_str ):
        if "(" in set_str or ") " in set_str:
            set_str = set_str.replace( '(', '')
            set_str = set_str.replace( ')', '')
            weight_str  = set_str.split(',')[0]
            rep_str     = set_str.split(',')[1]
        
            self.weight = self.get_weight( weight_str, WEIGHTS )
            self.repetitions, self.note = self.get_repetitions( rep_str )
        
        else:
            self.weight = None
            self.repetitions, self.note = self.get_repetitions( set_str )
            
    def __repr__( self ):
        return "Weight: {0} Repetitions: {1}".format( self.weight, self.repetitions )


    def get_repetitions( self,
                         rep_str ):
        
        notes = []    
        for (kw, kws) in [('PF', 'Poor form'), ('GF', 'Good form'), ('WP', 'Pauses')]:
            if kw in rep_str:
                rep_str = rep_str.replace( kw, '' )
                notes.append( kws )

        if 'x' in rep_str:
            rep = int( trim_ws( rep_str.split('x')[1] ) )
            repetitions = ( rep, rep )
        elif len( rep_str.split('-') ) > 1:
            repetitions = ( int( trim_ws( rep_str.split(' ')[0] ) ), int( trim_ws( rep_str.split(' ')[1] ) ) )
        else:
            repetitions = int( trim_ws( rep_str ) )
    
        return (repetitions, notes)


    def get_weight( self,
                    weight_str,
                    weights_dic):
        weight = 0
        for w in weight_str.split('+'):
            w = trim_ws( w )
            try:
                weight += float( w )
            except ValueError:
                weight += weights_dic[ w ]
        return weight




def get_weights_dic( weights_section ):
    dic = {}

    for weight in weights_section.splitlines():
        if len(weight) == 0 or weight[0] == '#':
            continue
        w = weight.split('=')
        
        try:
            dic[ trim_ws( w[0] ) ] = float( w[1] )
        except ValueError:
            dic[ trim_ws( w[0] ) ] = 0
            for wn in w[1].split( '+' ):
                dic[ trim_ws( w[0] ) ] += dic[ trim_ws( wn ) ]

    return dic

def get_weight_log_dic( weight_log_section ):
    dic = {}

    for date in weight_log_section.splitlines():
        if len(date) == 0 or date[0] == '#':
            continue
        w = date.split(':')
                
        dic[ trim_ws( w[0] ) ] = float( w[1] )
    return dic


def get_exercise_day( exercise_day_str ):

    split_list = exercise_day_str.splitlines()

    date = split_list[0]
    exercises = {}
    
    for ex in split_list[1:]:
        ex_name = ex.split(':')[0]
        sets    = ex.split(':')[1]

        if "note" in ex_name:
            exercises[ex_name] = sets
        else:
            exercises[ex_name] = []
            for _set in sets.split('-'):
                exercises[ex_name].append( ExerciseSet( _set ) )
    return ( date, exercises )


def get_exercise_log_dic( exercise_log_section ):

    dic = {}

    iline = iter( exercise_log_section.splitlines() )
    line = next(iline)

    exercise_day_str = ""
    
    while 1:
        if line in string.whitespace or len(line) == 0 or line[0] == '#':
            if exercise_day_str:
                exercise_day = get_exercise_day( exercise_day_str )
                dic[exercise_day[0]] = exercise_day[1]
            try:
                line = next(iline)
            except StopIteration:
                break
            exercise_day_str = ""
            continue

        exercise_day_str += line + '\n'

        try:
            line = next(iline)
        except StopIteration:
            if exercise_day_str:
                exercise_day = get_exercise_day( exercise_day_str )
                dic[exercise_day[0]] = exercise_day[1]
            break
    return dic

def print_exercise_dic( exercise_dic ):
    for date in exercise_dic.keys():
        print( date + ": ")
        for exercise in exercise_dic[date].keys():
            print( exercise + ": ")
            if "note" in exercise:
                print( exercise_dic[date][exercise] )
            else:
                for _set in exercise_dic[date][exercise]:
                    print( _set )
