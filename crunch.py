#!/usr/bin/python3

"""
crunch

Usage:
crunch.py <file>

"""

import docopt
import shutil, os, pickle
import string

from lib import ExerciseSet
import lib

if __name__ == "__main__":
    args = docopt.docopt( __doc__ )
    print( args )


if not os.path.isdir( "log_backup" ):
    os.mkdir( "log_backup" )
shutil.copyfile( args['<file>'], "log_backup/" + args['<file>'] ) 

#dic( date: exercise dic)
#exercise dic( exercise: [sets] )
#[sets] = [(repetitions, weight), (...)]
#[sets] = [(repetitions), (...)] #for calisthenics

log_file = open(args['<file>'], 'r')

WEIGHTS      = {}

WEIGHT_LOG   = {}

EXERCISE_LOG = {}


SECTIONS = {}

for section in log_file.read().split("SECTION:"):
    if not len(section):
        continue
    SECTIONS[ section.splitlines()[0].replace('-', '').replace(' ', '') ] = section.replace( section.splitlines()[0], '' )
    
print( SECTIONS )

WEIGHTS = lib.get_weights_dic( SECTIONS['WEIGHTS'] )
print( WEIGHTS )

WEIGHT_LOG = lib.get_weight_log_dic( SECTIONS['WEIGHTLOG'] )
print( WEIGHT_LOG )

EXERCISE_LOG = lib.get_exercise_log_dic( SECTIONS['EXERCISELOG'] )
lib.print_exercise_dic( EXERCISE_LOG )

print( EXERCISE_LOG )

print( "first: ", EXERCISE_LOG['17/05/2020'] )

pickle.dump( WEIGHTS,      open( "WEIGHTS.pickle",      "wb" ) )
pickle.dump( WEIGHT_LOG,   open( "WEIGHT_LOG.pickle",   "wb" ) )
pickle.dump( EXERCISE_LOG, open( "EXERCISE_LOG.pickle", "wb" ) )




"""

#N/9.81m^2
WEIGHT = {"09/04/2020": 76.0}



WEIGHTS = {"coat_hanger": 10,
           "table_metal": 10,
           "table_feet": 5,
           "bottle1.5": 1.5,
           "bike_chain": 2.5
}

WEIGHTS["h_bar_1"] = WEIGHTS["coat_hanger"] + WEIGHTS["table_metal"]
WEIGHTS["h_bar_2"] = WEIGHTS["coat_hanger"] + WEIGHTS["table_metal"] + WEIGHTS["table_feet"]
WEIGHTS["h_bar_3"] = WEIGHTS["coat_hanger"] + WEIGHTS["table_feet"]
WEIGHTS["h_bar_4"] = WEIGHTS["coat_hanger"] + WEIGHTS["table_feet"] + 2*WEIGHTS["bottle1.5"]


DATA  ={"31/03/2020": {"push_ups":  [15, 15, 15],\
                       "dips":      [15, 15, 15],\
                       "lunges":    [25, 25, 25] },
        "02/04/2020": {"leg_drops":   [8, 8, 8],\
                       "dumbell_row": [("h_bar_1", 2*15), ("h_bar_1", 2*15), ("h_bar_1", 2*17)],\
                       "squats":      [("h_bar_1", 20),   ("h_bar_1", 20),   ("h_bar_1", 20)] },
        "04/04/2020": {"superman":     [15, 15, 15],\
                       "good_morning": [("h_bar_1", 25), ("h_bar_1", 25), ("h_bar_1", 30)],\
                       "curls":        [("h_bar_1", 17), ("h_bar_1", 18), ("h_bar_1", 18), ("h_bar_1", 20)] },
        "06/04/2020": {"push_ups":   [16, 15, 13],\
                       "dips":       [15, 15, 17],\
                       "squats":     [("h_bar_2", 25), ("h_bar_2", 25), ("h_bar_2", 30)],\
                       "calf_raise": [("h_bar_2", 2*10), ("h_bar_2", 2*10), ("h_bar_2", 2*10)] },
        "08/04/2020": {"leg_drops":   [16, 16, 20],\
                       "dumbell_row": [("h_bar_2", 2*15), ("h_bar_2", 2*15), ("h_bar_2", 2*20)],\
                       "goodmorning": [("h_bar_2", 25),   ("h_bar_2", 25),   ("h_bar_2", 30)] },
        "10/04/2020": {"push_ups":   [15, 15, 15],\
                       "dips":       [("bike_chain", 20), ("bike_chain", 20), ("bike_chain", 20)],\
                       "squats":     [("h_bar_2", 25), ("h_bar_2", 25), ("h_bar_2", 30)],\
                       "curls":      [("h_bar_2", 12), ("h_bar_2", 12), ("h_bar_2", 13)] },
        "05/05/2020": {"push_ups":   [15, 15, 15],\
                       "dips":       [("bike_chain", 20), ("bike_chain", 20), ("bike_chain", 20)],\
                       "squats":     [("h_bar_3", 25), ("h_bar_3", 25), ("h_bar_3", 35)],\
                       "curls":      [("h_bar_3", 18), ("h_bar_3", 18), ("h_bar_3", 20)],
                       "note":        "First workout after injury"},
        "07/05/2020": {"leg_drops":   [16, 18, 22],\
                       "dumbell_row": [("h_bar_4", 2*15), ("h_bar_4", 2*15), ("h_bar_4", 2*18)],\
                       "goodmorning": [("h_bar_4", 25),   ("h_bar_4", 25),   ("h_bar_4", 45)] },
        "09/05/2020": {"squats":        [("h_bar_5", 25),   ("h_bar_5", 25),   ("h_bar_5", 40)],
                       "push_ups":      [15, 15, 19],\
                       "inclined_dips": [("bike_chain", 20), ("bike_chain", 19), ("bike_chain", 20)],\
                       "curls":    [("h_bar_4", 2*15), ("h_bar_4", 2*15), ("h_bar_4", 2*18)],\

}



        "09/05/2020": {"squats":   [25,   ("bike_chain", 25), ("bike_chain bottle1.5", 30)],
                       "push_ups": [("knee", 15) , ("knee", 10), ("poor form", 15)],\
                       "dips":     [ 10, 10, 10],\
                       "curls":    [("h_bar_4", 2*15), ("h_bar_4", 2*15), ("h_bar_4", 2*18)],\
                       "note": "regina"
"""
