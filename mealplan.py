#!/usr/bin/env python3

"""
meaplan.

Usage:
    mealplan add
    mealplan list
    mealplan get [ -c | --kcal <kcal> ] [ -p | --proteins <proteins> ] [ -h | --height <height> ] [ -w | --weight <weight> ]

Options:

-c --kcal        energy [kcal]
-p --proteins    proteins [g]
-h --height      height [cm]
-w --weight      weight [kg]

"""

import pickle
import docopt
import foods
from   foods import FoodFinder

#print( "Calories to be consumed in a week: {}".format( 7*args['<kcal>'] ) )
#print( "Proteins to be consumed in a week: {}".format( 7*args['<proteins>'] ) )

RECIPE_FILE = "RECIPES.pickle"
FOOD_FILE = "FOOD_DIC.pickle"

class Recipe:

    def __init__( self,
                  finder = FoodFinder( FOOD_FILE ),
                  name = ''):
        self.finder = finder
        self.name   = name
        self.ingredients = {}
        self.nutrition   = {}
                
    def add( self,
             ingredient,
             weight ):
        food_id = self.finder.search( ingredient )
        self.ingredients[ food_id ] = weight
        self.calc_nutrition()
        
    def calc_nutrition( self ):

        self.nutrition = {}
        
        for ingid, weight in self.ingredients.items():
            nutrients = self.finder.info( ingid )
            for nutrient, val in nutrients.items():
                try:
                    self.nutrition[ nutrient ][0] += val[0] * weight / 100.
                except:
                    self.nutrition[ nutrient ]  = [ val[0] * weight / 100., val[1] ]




    def list( self,
              micros = False ):
        for entry in self.food_dic.values():
            print( entry[0] )
            for nutrient in entry[1]:
                if not micros and not self.is_macro( nutrient ):
                    continue
                print("\t" + nutrient.name, nutrient.value, nutrient.unit)


                
    def __repr__( self ):
        string  = "{}:\n".format( self.name )
        string += "-Nutrition:\n"
        for key, item in self.nutrition.items():
            if self.finder.is_macro( key ):
                string += "    {}: {} {}\n".format( key, item[0], item[1] )
        string += "\n-Ingredients:\n"
        for key, item in self.ingredients.items():
            string += "    {0}: {1} g\n".format( self.finder.get_name( key ), item )
        return( string )


if __name__ == '__main__':
    
    args = docopt.docopt(__doc__)
    print(args)

    try:
        RECIPE_DIC = pickle.load( open( "RECIPES.pickle", "rb" ) )
    except:
        RECIPE_DIC = {}


    if args['add']:

        with FoodFinder( FOOD_FILE ) as myfinder:
            make_new = True
            while make_new:
                recipe_name = input( "Enter recipe name: " )
                while recipe_name in RECIPE_DIC.keys():
                    recipe_name = input( "Recipe name already taken. Please enter new recipe name: " )
                
                new_recipe = Recipe( myfinder,
                                     recipe_name )

                next_ingredient = input( "First ingredient: " ).lower()
                while next_ingredient not in set('nq'):
                    
                    while 1:
                        try:
                            next_weight = float( input( "{} weight: ".format( next_ingredient ) ).lower() )
                            break
                        except:
                            continue
                        
                    new_recipe.add( next_ingredient, next_weight )
                    next_ingredient = input( "Next ingredient or n/q to quit: " ).lower()
            
                RECIPE_DIC[ recipe_name ] = new_recipe
                make_new = input( "Make new recipe?" ) in 'yesoui'
            else:
                pickle.dump( RECIPE_DIC, open( "RECIPES.pickle", "wb" ) )
                        
    if args['list']:
        for key, item in RECIPE_DIC.items():
            RECIPE_DIC[ key ].calc_nutrition()
        print( RECIPE_DIC )
        



"""
MEAPLAN1 = {"Monday"   : [ BREAKFAST1,
                           LUNCH1,
                           DINNER1 ],
            "Tuesday"  :  [ BREAKFAST1,
                           LUNCH1,
                            DINNER1 ],
            "Wednesday":  [ BREAKFAST1,
                           LUNCH1,
                            DINNER1 ],
            "Thursday" :  [ BREAKFAST1,
                           LUNCH1,
                            DINNER1 ],
            "Friday"   :  [ BREAKFAST1,
                           LUNCH1,
                            DINNER1 ],
            "Saturday" :  [ BREAKFAST2,
                           LUNCH1,
                            DINNER1 ],
            "Sunday"   :  [ BREAKFAST2,
                           LUNCH1,
                           DINNER1 ]
            }
"""
