#!/usr/bin/env python3

"""
food.

Usage:
    food.py search <keyword> [-a | --all]
    food.py list [-a | --all]

Options:
-a --all   Lists all nutrients, or give all foods
"""

from docopt import docopt

#All nutritional information given per 100g
from usda import UsdaClient
import pickle
import os

api_key = "YfPUWaOguPl4djcmneBCLI6jWs8O8z51SNfOWf2l"
FOOD_FILE = "FOOD_DIC.pickle"

class FoodFinder:

    def __init__( self,
                  food_file ):
        
        self.client = UsdaClient( api_key )
        self.food_dic = {}
        self.food_file = food_file
        if os.path.isfile( self.food_file ):
            self.food_dic = pickle.load( open( self.food_file, 'rb' ) )

    def __enter__( self ):
        return self
            
    def info( self,
              food_id,
              verbose = False):
        
        info_dic = {}
        
        report = self.client.get_food_report( food_id )
        for nutrient in report.nutrients:
            info_dic[ nutrient.name ] = ( nutrient.value, nutrient.unit )

            if verbose:
                print(nutrient.name, nutrient.value, nutrient.unit)

        return info_dic

    def get_name( self,
                  food_id ):
        return self.food_dic[ food_id ][0]

    def search_dic( self,
                    keyword ):
        food_id = None
        for key, item in self.food_dic.items():
            if keyword in item[0]:

                answer = input( item[0] )
                if answer in set('yesoui'):
                    food_id = key
                    break
                elif answer.lower() == 'q':
                    exit()
        return food_id
    
    def search( self,
                keyword,
                limit = 100,
                reference = 'Standard Reference'):

        food_id = self.search_dic( keyword )
        
        if not food_id:
            result = self.client.search_foods( keyword,
                                               limit,
                                               ds = reference)
            while 1:
                food = next(result)
                answer = input( food )
                if answer in set('yesoui'):
                    break
                elif answer.lower() == 'q':
                    exit()
                
            self.food_dic[ food.id ] = ( food.name, self.info( food.id ) )
            food_id = food.id
            
        return food_id


    def add( self,
             name ):
        #add a food 
    
    def is_macro( self,
                  nutrient_name ):
        macro_list = ["water",
                      "energy",
                      "protein",
                      "fat",
                      "carbohydrate",
                      "fiber",
                      "sugar"]
        for macro in macro_list:
            if macro in ( nutrient_name ).lower():
                return True
        return False



    def list( self,
              micros = False ):
        for entry in self.food_dic.values():
            print( entry[0] )
            for nutrient, nutrient_info in entry[1].items():
                if not micros and not self.is_macro( nutrient ):
                    continue
                print( "\t{}: {} {}".format( nutrient, nutrient_info[0], nutrient_info[1] ) )

        
    def __exit__( self, *a ):
        pickle.dump( self.food_dic, open( self.food_file, 'wb' ) )
        print( "Wrote out dictionary to {}".format( self.food_file ) )


if __name__=='__main__':

    args = docopt( __doc__ )
    print(args)

    with FoodFinder( FOOD_FILE ) as myfoodfinder:
        
        if args['search']:
            
            reference = 'Standard Reference'
            
            if args['--all']:
                reference = None
            
            food = myfoodfinder.search( args['<keyword>'],
                                        reference = reference)
    
        if args['list']:
        
            myfoodfinder.list( args['--all'] )





#api_endpoint = "https://developer.nrel.gov/api/alt-fuel-stations/v1/"


#https://developer.nrel.gov/api/alt-fuel-stations/v1/nearest.json?api_key=YfPUWaOguPl4djcmneBCLI6jWs8O8z51SNfOWf2l&location=Denver+CO


#res = requests.get( api_endpoint,
#                    headers={'Authorization': 'api_key YfPUWaOguPl4djcmneBCLI6jWs8O8z51SNfOWf2l'} )

#print(res.status_code)
